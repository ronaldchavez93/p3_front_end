var app = angular.module("tutenModule", ["ngRoute", "validationsModule"]);

app.config(["$routeProvider", "$locationProvider", function(routeProvider, locationProvider) {
    locationProvider.hashPrefix('');
    routeProvider
		.when('/dashboard', {
			templateUrl: 'app/dashboard.html',
            controller: 'dashboradController',
            CaseInsensitiveMatch: true
		})
        .when('/', {
			templateUrl: 'app/login.html',
			controller: 'loginController'
		})
		.otherwise({
			redirectTo: '/'
        });
        
      
}]);


app.controller("loginController", ["$scope", "$location", "$http", "validations", function(scope, location, http, validations){
    /* VARIABLES SCOPE */
    scope.user = {};
    scope.btnLogin = false;
    /* FIN */

    /* VALIDACIONES */
    scope.validations = validations;
    /* FIN */


    /* METODO QUE CONSUME EL API REST PARA INICIAR SESION */
    scope.handleLogin = function(){
        let email = scope.user.email;
        let header = {  'Accept': 'application/json',
                        password: scope.user.password,
                        app: 'APP_BCK'
                        
                    };
        scope.btnLogin = true;
        http.put(`https://dev.tuten.cl/TutenREST/rest/user/${email}`,{}, {
            headers: header
        })
            .then(function (response){
                console.log(response);
                let data = response.data;
                /*Add localStorage*/
                localStorage.setItem('_access_token_', data.sessionTokenBck);
                /* Fin */
                /* ir a la vista dashboard */
                location.path('dashboard');
                /* Fin */
                scope.btnLogin = false;
            },function (error){
                console.log(err);
                scope.btnLogin = false;
            });
    };
    /* FIN */

    /* LIMPIAR FORMULARIO */
    scope.reset = function(form) {
        scope.user = {};

        if (form) {
          form.$setPristine();
          form.$setUntouched();
        }
    };
    /* FIN */
}]);

app.controller("dashboradController", ["$scope", "$location", "$http", "validations", function(scope, location, http, validations){

    /* VARIABLES SCOPE */
    scope.customers = [];
    scope.listCustomers = [];
    scope.bookingId = '';
    scope.bookingPriceMin = '';
    scope.bookingPriceMax = '';
    scope.itemFilter = 'BookingId';
    /* FIN */

    let token = localStorage.getItem('_access_token_') || '';

    /* VALIDACIONES */
    scope.validations = validations;
    /* FIN */


    if(token == ''){
        location.path('/'); 
        return;
    }

    let header = {  'Accept': 'application/json',
                    adminemail: 'testapis@tuten.cl',
                    app: 'APP_BCK',
                    token: token
                };
    
    http.get(`https://dev.tuten.cl/TutenREST/rest/user/contacto@tuten.cl/bookings?current=true`, {
        headers: header
    })
    .then(function (response){
        console.log(response);
        scope.customers = response.data;
        scope.listCustomers = response.data;
    },function (error){
        console.log(err);
    });
    
    scope.searchCustomerBookingId = function(){
        console.log(scope.bookingId);
        let query = scope.bookingId;
        if(query.length > 0){

            console.log(scope.listCustomers);
            scope.customers = scope.listCustomers.filter(  data => 
                                                                data.bookingId.toString().indexOf(query) > -1 );
            }else{
            scope.customers = scope.listCustomers;
        }
    }

    scope.searchCustomerbookingPrice = function(){
        console.log(scope.bookingPriceMin);
        console.log(scope.bookingPriceMax);
        let queryMin = scope.bookingPriceMin.replace(",", "");
        let queryMax = scope.bookingPriceMax.replace(",", "");
        if(queryMin.length > 0 && queryMax.length > 0){

            if(queryMin > queryMax){
                return;
            }

            console.log(scope.listCustomers);
            scope.customers = scope.listCustomers.filter(  data => 
                                                                (data.parentBooking == undefined ? 0 : data.parentBooking.bookingPrice)  >=  queryMin && 
                                                                (data.parentBooking == undefined ? 0 : data.parentBooking.bookingPrice) <=  queryMax );
            
        }else if(queryMin.length > 0){
            console.log(scope.listCustomers);
            scope.customers = scope.listCustomers.filter(  data => 
                                                                (data.parentBooking == undefined ? 0 : data.parentBooking.bookingPrice)  >=  queryMin );
        }else if(queryMax.length > 0){
            scope.customers = scope.listCustomers.filter(  data => 
                                                                (data.parentBooking == undefined ? 0 : data.parentBooking.bookingPrice) <=  queryMax );
        }else{
            scope.customers = scope.listCustomers;
        }
    }

    scope.selectFilter = function(){
        
        scope.bookingId = '';
        scope.bookingPriceMin = '';
        scope.bookingPriceMax = '';
        scope.customers = scope.listCustomers;
    }

    scope.handleLogout = function(){

        /* Borrar la variable localStorage */
        localStorage.removeItem('_access_token_');
        /* ir a la vista login */
        location.path('/');
        
        return false;
    }


}]);