var app = angular.module("validationsModule", []);
app.factory('validations', function() {
    var validationSvc = {};

    /* VALIDACION SOLO NUMEROS */
    validationSvc.onlyNumbers = function (e) {
        let key = e.keyCode || e.which;
        let tecla = String.fromCharCode(key).toLowerCase();
        let letras = "0123456789";

        if(letras.indexOf(tecla) == -1 && key != 8)
            e.preventDefault();
    }
    /* FIN */

    /* VALIDACION PARA CAMPOS DE PRECIOS */
    validationSvc.onlyPrices = function (e) {
        let key = e.keyCode || e.which;
        let tecla = String.fromCharCode(key).toLowerCase();
        let letras = "0123456789,";

        if(letras.indexOf(tecla) == -1 && key != 8)
            e.preventDefault();
    }
    /* FIN */

    return  validationSvc;
});